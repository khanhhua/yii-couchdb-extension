<?php
/**
 * Description of CCouchDbView
 * @link http://wiki.apache.org/couchdb/HTTP_view_API
 * @author Khanh Hua
 */
class CCouchDbView {
  /**
   * Name of the design document
   * @var string
   */
	public $name;
  /**
   * Name of the views which this document supports
   * @var string 
   */
	public $view = 'all';
	
	public $key;
	public $keys;
	public $startkey;
	public $endkey;
	
	public $limit;
	
	public function getPath() {
		return "_design/$this->name/_view/$this->view";		
	}
	
	public function getQuery(){
		$query_data = array();
		
		if (!empty($this->key)){
			$query_data[] = "key=$this->key";
		} else if (!empty ($this->keys)) {
			$query_data[] = "keys=[".  join(',', $this->keys)."]";
		}
		
		if (!empty($this->startkey)){
			$query_data[] = "startkey=$this->startkey";
		}
		if (!empty ($this->endkey)) {
			$query_data[] = "endkey=$this->endkey";
		}
		
		if (!empty($this->limit)) {
			$query_data[] = "limit=$this->limit";
		}
		
		return join('&',$query_data);
	}
}

?>
