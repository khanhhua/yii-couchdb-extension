<?php
/**
 * Description of CCouchDbRequest
 *
 * @author khanhhua
 */
class CCouchDbRequest {
	const CONTENT_TYPE_DATABASE = 1;
	const CONTENT_TYPE_DOCUMENT = 2;
	const CONTENT_TYPE_VIEW = 3;
	
	const OPERATION_CREATE = 1;
	const OPERATION_RETRIEVE = 2;
	const OPERATION_UPDATE = 3;
	const OPERATION_DELETE = 4;
	
	const VERB_POST = 'POST';
	const VERB_GET = 'GET';
	const VERB_PUT = 'PUT';
	const VERB_DELETE = 'DELETE';
	
	const SPECIAL_URI_ALL_DOCS = '_all_docs';
	const SPECIAL_URI_UUIDS = '_uuids';
	/**
   * Value can be one of RESTful
   * @var string 
   */
  public $operation;
	/**
	 *
	 * @var string One of DOCUMENT 
	 */
  public $content_type;
	/**
	 * Can be an ID or a CDocumentModel
	 * @var mixed 
	 */
	public $data;
	
	public $query;
  /**
   * @var CCouchDbConnection
   */
  private $_connection;
  private $_data;
	private $_url;
	private $_verb;
  /**
	 * 
	 * @param type $connection
	 * @param type $method
	 * @param type $content
	 */
  public function __construct($connection) {
    $this->_connection = $connection;
	}
  
  public function send($data, $query = '',
					$operation = self::OPERATION_RETRIEVE, 
					$content_type = self::CONTENT_TYPE_DOCUMENT){
    $this->data = $data;
		$this->query = $query;
		$this->operation = $operation;
    $this->content_type = $content_type;
		
    $this->prepare();
    $this->_connection->open($this);
    $response = $this->_connection->send();
    $this->_connection->close();
    return $response;
  }
  
	public function getVerb(){
		return $this->_verb;
	}
	
	public function getUrl() {
		return $this->_url;
	}

	public function getEncodedData() {
		return $this->_data;
	}
	
	private function prepare(){
    if ($this->content_type == self::CONTENT_TYPE_DOCUMENT) {
      switch ($this->operation) {
          case self::OPERATION_CREATE:
            $this->_verb = self::VERB_POST;
            $this->_url = $this->_connection->getDatabaseURL();
            if (is_a($this->data,'CDocumentModel')) {
              $data = CJSON::decode(CJSON::encode($this->data));
              unset($data['_id']);
              unset($data['_rev']);
              $this->_data = CJSON::encode($data);
            } else {
              $this->_data = CJSON::encode($this->data);
            }         
          break;
          case self::OPERATION_RETRIEVE:
            $this->_verb = self::VERB_GET;
            $this->_url = $this->_connection->getDatabaseURL();
            if (is_string($this->data))
              $this->_url .= '/'.$this->data;
            else if (is_object($this->data))
              $this->_url .= '/'.$this->data->_id;
            else 
              throw new CException('Invalid data for this method');

            if (!empty($this->query))
              $this->_url .= '?'.$this->query;

          break;
          case self::OPERATION_UPDATE:
            $this->_verb = self::VERB_PUT;
            $this->_url = $this->_connection->getDatabaseURL();
            if (is_object($this->data))
              $this->_url .= '/'.$this->data->_id;
            else
              throw new CException('Invalid data for this method');
            $data = CJSON::decode(CJSON::encode($this->data));
            unset($data['_id']);
            
            $this->_data = CJSON::encode($data);
            break;
          case self::OPERATION_DELETE:
            $this->_verb = self::VERB_DELETE;
            $this->_url = $this->_connection->getDatabaseURL();
            if (is_object($this->data))
              $this->_url .= '/'.$this->data->_id;
            else
              throw new CException('Invalid data for this method');

            break;
			}
		} else if ($this->content_type == self::CONTENT_TYPE_VIEW) {
			switch ($this->operation) {
				case self::OPERATION_RETRIEVE:
					$this->_verb = self::VERB_GET;
					$this->_url = $this->_connection->getDatabaseURL();
					// TODO: Factor generic data for view
					if (!is_a($this->data, 'CCouchDbView')) 
						throw new CException('Invalid data for this method');
					
					$this->_url .= '/'.$this->data->getPath();
					$this->_url .= '?'.$this->data->getQuery();
					break;
			}
		} else if ($this->content_type == self::CONTENT_TYPE_DATABASE) {
			
		}
  }
}
?>
