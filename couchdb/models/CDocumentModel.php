<?php
/**
 * Description of CDocumentModel
 *
 * @author khanhhua
 */
class CDocumentModel extends CModel {
  private static $couchdb = null;
  
  public $_id = null;
  public $_rev = null;
  public $doc_type;
  
	protected $_dynamic_data = array();

	private static $_names = array('content', 'level', 'type');
	
  public function __set($name, $value) {
    if (property_exists(get_class($this), $name)) {
      parent::__set($name, $value);
    } else if (in_array($name, self::$_names)) {
			$this->_dynamic_data[$name] = $value;
		} 
	}
  
  public function __get($name) {
    if (property_exists(get_class($this), $name)) {
      return parent::__get($name);
    } else if (array_key_exists($name, $this->_dynamic_data)) {
			return $this->_dynamic_data[$name];
		} else
      throw new CException('Cannot access non-existing property');
  }
	/**
   * 
   * @return CCouchDbConnection
   */
  public function getDbConnection(){
    return self::getCouchDbConnection();
  }
  /**
   * 
   * @return array Array of static and dynamic attributes
   */
  public function attributeNames() {
    return array_merge(array('_id','_rev','doc_type'), self::$_names);
  }
  /**
   * List all available revisions
   */
  public function revisions(){
    
  }
  
  public function loadRevision($revision = null){
    
  }
	
	public static function find($id, $revision = '') {
		$req = self::getCouchDbConnection()->request();
		if (!empty($revision))
			$res = $req->send($id, "rev=$revision");
		else
			$res = $req->send($id);
		$data = $res->getData();
		$doc = new CDocumentModel();
		
		$doc->setAttributes((array)$data, false);
		
		return $doc;
	}
	
	public static function findAll($condition=array()) {
		$req = self::getCouchDbConnection()->request();
		$res = $req->send(CCouchDbRequest::SPECIAL_URI_ALL_DOCS);
		$data = $res->getData();
		
		if (isset($data->rows)) {
			$docs = array();
			foreach($data->rows as $row) {
				$doc = new CDocumentModel();
				$doc->setAttributes((array)$row, false);
				$docs[] = $row;
			}
			
			return $docs;
		}
		else
			return false;
	}
  /**
   * Save a document. Adapter determines whether to create new or update an 
   * existing version
   * @return boolean
   */
  public function save() {
    return $this->getDbConnection()->saveDocument($this);
  }
    
  protected static function getCouchDbConnection(){
      if (self::$couchdb !== null)
          return self::$couchdb;
      else
      {
          self::$couchdb = Yii::app()->couchdb;
          if (self::$couchdb instanceof CCouchDbConnection)
          {
              self::$couchdb->setActive(true);
              return self::$couchdb;
          }
          else
              throw new CDbException(Yii::t('yii','Document Model requires a "couchdb" CCouchDbConnection application component.'));
      }
  }  
}

?>
