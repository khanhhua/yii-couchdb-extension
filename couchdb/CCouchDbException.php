<?php
/**
 * Description of CCouchDbException
 *
 * @author khanhhua
 */
class CCouchDbException extends CDbException {
  public function __construct($message, $code = 0, $errorInfo = null) {
    parent::__construct($message, $code, $errorInfo);
  }
}

?>
