<?php
/**
 * Wrapper for responses from CouchDB Server
 *
 * @author khanhhua
 */
class CCouchDbResponse {
  const STATUS_UNKNOWN = 0;
  const STATUS_OK = 'OK';
  const STATUS_ERROR = 'ERROR';
  
  const CONTENT_TYPE_SERVER = 1;
  const CONTENT_TYPE_DATABASE = 2;
	const CONTENT_TYPE_DOCUMENT = 3;
	const CONTENT_TYPE_DOCUMENT_LIST = 31;
	// const CONTENT_TYPE_VIEW = 3;
	
	private $_connection;
  private $_raw_data;
  private $_data;
  private $_content_type;
  private $_status = self::STATUS_UNKNOWN;
  
  public function __construct($connection, $raw_data, $content_type = self::CONTENT_TYPE_DOCUMENT) {
    $this->_connection = $connection;
    $this->_raw_data = $raw_data;
    $this->_content_type = $content_type;
    $this->parse();
  }
  
  public function getData(){
    return $this->_data;
  }
  
  public function getStatus() {
    return $this->_status;
  }
  
  private function parse(){
    $this->_data = json_decode($this->_raw_data);
    
    if ($this->_data->ok)
      $this->_status = self::STATUS_OK;
    
    if (isset($this->_data->total_rows)) {
      $this->_content_type = self::CONTENT_TYPE_DOCUMENT_LIST;
      $this->_status = self::STATUS_OK;
    } else {
      $this->_content_type = self::CONTENT_TYPE_DOCUMENT;
      $this->_status = self::STATUS_OK;
    }
  }
}

?>
