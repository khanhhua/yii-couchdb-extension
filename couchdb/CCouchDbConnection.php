<?php
/**
 * Description of CCouchDbConnection
 *
 * @author khanhhua
 */
class CCouchDbConnection extends CApplicationComponent {
  public $connectionString;
  public $username='';
  public $password='';
  public $autoConnect = true;
  public $ssl = false;
	
  public $active = false;
  public $host = '';
	public $port = '';
  public $db = '';
  /**
	 * 
	 * @param string $dsn couchdb://root:root@localhost:5984/quiz
	 * @param type $username
	 * @param type $password
	 */
  public function __construct($dsn='',$config=array())
	{
		$this->connectionString=$dsn;
		
		if (!empty($config['username']))
			$this->username = $config['username'];
		
		if (!empty($config['password']))
			$this->password = $config['password'];
	}
  
  public function init() {
    if (preg_match('#couchdb:\/\/(?P<user>\w+):(?P<pass>.+?)@(?P<host>.+?):(?P<port>[0-9]+)/(?P<db>.+)$#', 
						$this->connectionString, $matches)) {
			$this->host = $matches['host'];
			$this->db = $matches['db'];
			$this->port = $matches['port'];
			$this->username = $matches['user'];
			$this->password = $matches['pass'];
		}
		
		$this->autoConnect = true;
		parent::init();
  }
	
  public function createDatabase(){
    
  }
  /**
   * 
   * @param CDocumentModel $document
   */
  public function saveDocument(CDocumentModel $document){
    $req = $this->request();
    // Create new
    if (empty($document->_id)){
      $response = $req->send($document, '', CCouchDbRequest::OPERATION_CREATE, CCouchDbRequest::CONTENT_TYPE_DOCUMENT);
      if ($response->getStatus() == CCouchDbResponse::STATUS_OK){
        $data = $response->getData();
        $document->_id = $data->id;
        $document->_rev = $data->rev;
        return true;
      } else
        return false;
    } else { // Update
      $response = $req->send($document, '', CCouchDbRequest::OPERATION_UPDATE, CCouchDbRequest::CONTENT_TYPE_DOCUMENT);
      if ($response->getStatus() == CCouchDbResponse::STATUS_OK){
        $data = $response->getData();
        $document->_rev = $data->rev;
        return true;
      } else
        return false;
    }
  }
  /**
	 * 
	 * @return \CCouchDbRequest
	 */
  public function request(){
    return new CCouchDbRequest($this);
  }
  
  public function setActive($value)
	{
		if($value!=$this->active){
			$this->active = $value;
		}
  }
  
  public function getHost(){
    return $this->host;
  }
  
  public function getDatabaseURL(){
		if ($this->ssl)
			return 'https://'.$this->host.':'.$this->port.'/'.$this->db;
		else
			return 'http://'.$this->host.':'.$this->port.'/'.$this->db;
  }
  
  private $_ch = null;
	/**
	 * @param CCouchDbRequest $request
	 */
  public function open($request) {
    $this->_ch = curl_init($request->getUrl());
		switch ($request->getVerb()) {
			case CCouchDbRequest::VERB_POST: curl_setopt($this->_ch, CURLOPT_POST, 1);
        $post_data = $request->getEncodedData();
				curl_setopt($this->_ch, CURLOPT_POSTFIELDS, $post_data);
        curl_setopt($this->_ch, CURLOPT_HTTPHEADER,
            array('Content-Type: application/json', 'Content-length: '.strlen($post_data)));
				break;
			case CCouchDbRequest::VERB_PUT: curl_setopt($this->_ch, CURLOPT_CUSTOMREQUEST, 'PUT'); 
				curl_setopt($this->_ch, CURLOPT_POSTFIELDS, $request->getEncodedData());
				break;
			case CCouchDbRequest::VERB_DELETE: curl_setopt($this->_ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
				break;
			case CCouchDbRequest::VERB_GET: curl_setopt($this->_ch, CURLOPT_HTTPGET, 1); 
				break;	
		}
		curl_setopt($this->_ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($this->_ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC );
		curl_setopt($this->_ch, CURLOPT_USERPWD, $this->username.':'.$this->password);
  }
  
  public function close() {
    curl_close($this->_ch);
  }
  public function send() {
		$response_data = curl_exec($this->_ch);
		$response = new CCouchDbResponse($this, $response_data);
		return $response;
  }
}

?>
